<?php get_header(); 

/* Template Name: Contato */

$tituloBox = get_field('titulo_fale_conosco', 'option');
$textoBox = get_field('texto_fale_conosco', 'option');
$iframe = get_field('iframe_mapa', 'option');

$tituloFaca = get_field('titulo_faca_parte', 'option');
$textoFaca = get_field('texto_faca_parte', 'option');
?>	
    <main>
        <?php include 'modulos/header-page.php'; ?>
        <section class="container">
            <section class="content">
                <h2 class="titulo titulo-fale-conosco">Fale Conosco</h2>
                <form id="formContato" class="formContato">
                    <div class="content">
                        <input type="text" name="nome" placeholder="Nome*" required>
                        <div class="row">
                            <input type="email" name="email" placeholder="Email*" required>
                            <input type="text" name="assunto" placeholder="Assunto*" required>
                        </div>
                        <div class="row">
                            <select name="selectEstadoOrigem" id="" class="selectEstados" tabindex="-1" required>
                                <option value="" disabled selected>Estado</option>
                            </select>
                            <select name="selectCidadeOrigem" id="" class="selectCidades" tabindex="-1" required>
                                <option value="" disabled selected>Cidade</option>
                            </select>
                        </div>
                        <textarea name="mensagem" id="" cols="30" rows="10" placeholder="Mensagem"></textarea>
                        <div class="button">
                            <button class="btn">Enviar</button>
                        </div>
                        <div id="mensagem" class="hidden-xs hidden-lg" style="color:#fff;font-size:18px;">Formulário Enviado com sucesso!</div>
                    </div>
                </form>
            </section>
            <aside class="aside">
                <div class="box">
                    <h3 class="titulo titulo-box"><?php echo $tituloBox; ?></h3>
                    <p><?php echo $textoBox; ?></p>
                    <?php echo $iframe; ?>
                </div>
                <div class="box faca-parte">
                    <h3 class="titulo titulo-box titulo-faca-parte"><?php echo $tituloFaca; ?></h3>
                    <p><?php echo $textoFaca; ?></p>
                </div>
            </aside>
        </section>
    </main>
    <input type="hidden" id="jsonCidades" value="<?php echo get_template_directory_uri(); ?>/js/cidades.json">
<?php get_footer(); ?> 
