<?php

$banner = get_field('banner', $post->ID);
?>

<section id="bannerHome" class="banner-home">
    <div class="sliders">

        <?php foreach ($banner as $slide) {
            $imgDesk = $slide['imagem_desktop'];
            $imgMobile = $slide['imagem_mobile'];
            $frase = $slide['frase_principal'];
            $texto = $slide['texto_de_apoio'];
            $tipoBotao = $slide['tipo_link'];
            if($tipoBotao == 1){
                $link = $slide['link_interno'];
                $target = false;
            }else{
                $link = $slide['link_externo'];
                $target = true;
            }
        ?>
        <div class="slide">
            <div class="bg">
                <div class="filtro"></div>
                <img src="<?php echo $imgDesk; ?>" class="hidden-xs">
                <img src="<?php echo $imgMobile; ?>" class="hidden-lg">
            </div>
            <div class="content">
                <h1><?php echo $frase; ?></h1>
                <h2><?php echo $texto; ?></h2>
                <a href="<?php echo $link; ?>" id="botao" class="btn outline" <?php if($target){ echo 'target="_blank"'; } ?>>Saiba mais</a>
            </div>
        </div>
        <?php } ?>
    </div>
    </section>
