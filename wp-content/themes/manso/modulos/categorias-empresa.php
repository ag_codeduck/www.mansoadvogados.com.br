<div class="categorias">

    <?php 
                    $argCategoria = array(
                        'post_type'  => 'page',
                        'posts_per_page' => -1,
                        'order' => 'ASC',
                        'orderby' => 'menu_order',
                        'post_parent' => $id
                    );
                    $categoria = new WP_Query( $argCategoria );
    
                    if( $categoria->have_posts() ): while( $categoria->have_posts() ) : $categoria->the_post();
    
                    $backgroundDesk = get_field("background_categoria_desk");
                    $backgroundMobile = get_field("background_categoria_mobile");
                    $link = get_the_permalink();
                    $titulo = get_the_title();
                    
                ?>
            <div class="card-categoria">
                <div class="bg">
                    <div class="filtro"></div>
                    <img src="//localhost/wp-content/uploads/2019/10/bg-header-desk.jpg" alt="STÉARINERIE DUBOIS">
                    <!-- <img src="<?php echo $bgDesktop; ?>" class="hidden-xs" alt="<?php echo $title; ?>">
                    <img src="<?php echo $bgMobile; ?>" class="hidden-lg" alt="<?php echo $title; ?>"> -->
                </div>
                <h2><?php echo $titulo; ?></h2>
                <a href="<?php echo $link; ?>" class="btn outline">Saiba mais</a>
            </div>
            <?php
                    endwhile; endif;
                    wp_reset_query();
                ?>            
</div>
