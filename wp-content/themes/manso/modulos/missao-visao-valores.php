<?php

$missao = get_field("texto_missao", $post->ID);
$visao = get_field("texto_visao", $post->ID);
$valores = get_field("texto_valores", $post->ID);
?>

<section id="missao-visao-valores">
    <div class="box boxMVV">
        <h3 class="titulo sidebar">Missão</h3>
        <?php foreach ($missao as $item) { 
            $itemMissao = $item['texto_Missao'];
        ?>
        <p><?php echo $itemMissao; ?></p>
        <?php } ?>
    </div>
    <div class="box boxMVV">
        <h3 class="titulo sidebar">Visão</h3>
        <p><?php echo $visao; ?></p>
    </div>
    <div class="box boxMVV">
        <h3 class="titulo sidebar">Valores</h3>
        <ul>
            <?php foreach ($valores as $itemV) { 
                $itemValores = $itemV['texto_valor'];
            ?>
            <li><?php echo $itemValores; ?></li>
            <?php } ?>
        </ul>
    </div>
</section>