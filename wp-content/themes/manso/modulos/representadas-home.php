<section class="container representadas-home">
    <h2>Representadas</h2>
    <section class="content-full representadas">
        <?php 
            $argRepresentadas = array(
                'post_type'  => 'page',
                'posts_per_page' => -1,
                'order' => 'ASC',
                'orderby' => 'menu_order',
                'post_parent' => 21
            );
            $representadas = new WP_Query( $argRepresentadas );

            if( $representadas->have_posts() ): while( $representadas->have_posts() ) : $representadas->the_post();

            $logoEmpresa = get_field('logo_empresa', $postID);
            $textoAdicional = get_field('texto_empresa', $postID);
            $link = get_the_permalink();
            $titulo = get_the_title();
            
        ?>
        <a class="card-empresa" href="<?php echo $link; ?>">
            <img src="<?php echo $logoEmpresa; ?>">
            <div class="hover">
                <p><?php echo $textoAdicional; ?></p>
            </div>
        </a>
        <?php
            endwhile; endif;
            wp_reset_query();
        ?>            
    </section>
</section>