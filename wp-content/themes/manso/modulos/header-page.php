<?php
    $title = get_the_title();
    $bgDesktop = get_field('header_desktop', $post->ID);
    $bgMobile = get_field('header_mobile', $post->ID);
?>

<header class="header-page">
    <div class="bg">
        <img src="<?php echo $bgDesktop; ?>" class="hidden-xs" alt="<?php echo $title; ?>">
        <img src="<?php echo $bgMobile; ?>" class="hidden-lg" alt="<?php echo $title; ?>">
    </div>
    <h1 class="titulo header"><?php echo $title; ?></h1>
    <?php include 'breadcrumb.php'; ?>
</header>