<?php

$imgDeskDestaque = get_field('imagem_desktop_destaque', $post->ID);
$imgDeskMobileDestaque = get_field('imagem_mobile_destaque', $post->ID);
$tituloDestaque = get_field('titulo_destaque', $post->ID);
$textoDestaque = get_field('texto_destaque', $post->ID);
$linkDestaque = get_field('link_destaque', $post->ID);
?>

<section class="banner-destaque">
    <div class="bg">
        <div class="filtro"></div>
        <img src="<?php echo $imgDeskDestaque; ?>" class="hidden-xs">
        <img src="<?php echo $imgDeskMobileDestaque; ?>" class="hidden-lg">
    </div>
    <div class="content">
        <h1><?php echo $tituloDestaque; ?></h1>
        <h2><?php echo $textoDestaque; ?></h2>
        <a href="<?php echo $linkDestaque; ?>" id="botao" class="btn outline" <?php if($target){ echo 'target="_blank"'; } ?>>Saiba mais</a>
    </div>
</section>
