<section id="outros-canais">
    <h3 class="titulo sidebar">Outros <span>Canais</span></h3>

    <div class="canais">
    <?php 
                $argVeiculos = array(
                    'post_type'  => 'veiculos',
                    'posts_per_page' => -1,
                    'order' => 'ASC',
                    'orderby' => 'menu_order',
                    'post__not_in' => array($id)
                );
                $veiculos_query = new WP_Query( $argVeiculos );

                if( $veiculos_query->have_posts() ): while( $veiculos_query->have_posts() ) : $veiculos_query->the_post();

                $logoVeiculo = get_field("logo_veiculo");
                $link = get_the_permalink();
                $titulo = get_the_title();
                
            ?>
        <div class="card-veiculo">
            <img class="img-veiculo" src="<?php echo $logoVeiculo; ?>" alt="<?php echo $titulo; ?>">
            <div class="hover">
                    <a href="<?php echo $link; ?>" class="saiba">saiba mais</a>
            </div>
        </div>
        <?php
                endwhile; endif;
                wp_reset_query();
            ?>            

    </div>
</section>