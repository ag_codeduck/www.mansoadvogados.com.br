<?php
    $tituloFormulas = get_field('titulo_formula', $post->ID);
    $textoFormulas = get_field('texto_formula', $post->ID);
    $linkFormulas = get_field('link_formula', $post->ID);
?>

<section class="formulas-home">
    <img data-svg="<?php echo get_template_directory_uri(); ?>/img/folha-bg.svg" class="svg"></a>
    <h2><?php echo $tituloFormulas; ?></h2>
    <p><?php echo $textoFormulas; ?></p>
    <a href="<?php echo $linkFormulas; ?>" class="btn outline yellow">Saiba mais</a>
</section>