<?php get_header(); 

/* Template Name: Atuação */

$tituloBox = get_field('titulo_fale_conosco', 'option');
$textoBox = get_field('texto_fale_conosco', 'option');


while ( have_posts() ) : the_post();   
    $title = get_the_title();
    $frase = get_field('texto_frase_destaque', $post->ID);
    
endwhile;       

?>	
    <main>
        <?php include 'modulos/header-page.php'; ?>
        <section class="container">
            <section class="content">
                <section class="frase">
                    <h2 class="titulo titulo-frase"><?php echo $frase; ?></h2>
                </section>
                <?php the_content(); ?>
            </section>
            <aside class="aside">
                <div class="box">
                    <h3 class="titulo titulo-box"><?php echo $tituloBox; ?></h3>
                    <p><?php echo $textoBox; ?></p>
                </div>
            </aside>
        </section>
    </main>
<?php get_footer(); ?> 
