<?php get_header(); 

/* Template Name: Quem Somos */

$tituloValores = get_field('titulo_valores', $post->ID);
$valores = get_field('itens_valores', $post->ID);

while ( have_posts() ) : the_post();   
    $title = get_the_title();
    
endwhile;       

?>	
    <main>
        <?php include 'modulos/header-page.php'; ?>
        <section class="container">
            <section class="content escritorio">
                <?php the_content(); ?>
            </section>
            <aside class="aside valores">
                <div class="box">
                    <h3 class="titulo titulo-box"><?php echo $tituloValores; ?></h3>
                    <?php 
                        foreach ($valores as $valor) {
                            $icone = $valor['icone'];
                            $titulo = $valor['titulo'];
                            $texto = $valor['texto'];
                    ?>
                    <div class="valor">
                        <img data-svg="<?php echo $icone; ?>" class="svg">
                        <div class="content-valor">
                            <h3 class="titulo titulo-valores"><?php echo $titulo; ?></h3>
                            <p><?php echo $texto; ?></p>
                        </div>
                    </div>
                    <?php
                        } 
                    ?>
                </div>
            </aside>
        </section>
    </main>
<?php get_footer(); ?> 
