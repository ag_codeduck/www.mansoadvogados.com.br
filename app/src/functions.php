<?php 

//Padrões
include_once("action/options/box-fale-conosco.php");
include_once("action/options/functions.php");
include_once("action/options/header.php");
include_once("action/options/favicon.php");
include_once("action/options/redes-sociais.php");


//Módulos Templates
include_once("action/acf/destaque-home.php");
include_once("action/acf/frase-destaque.php");
include_once("action/acf/header-page.php");
include_once("action/acf/slide-home.php");
include_once("action/acf/valores.php");


//removendo admin bar do theme

show_admin_bar(false);

//adicionando svg no upload de imagens

function cc_mime_types($mimes) {
       $mimes['svg'] = 'image/svg+xml';
       return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//removendo Infos Header
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

wp_deregister_script('jquery');
?>