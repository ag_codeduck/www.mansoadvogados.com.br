const inputJsonCidades = document.getElementById('jsonCidades');
if(inputJsonCidades){
    (function encontraEstados(){
        document.addEventListener('DOMContentLoaded', e=>{
            let jsonEstados;
            let request = new XMLHttpRequest();
                request.open('GET', inputJsonCidades.value , true);
                request.onload = function() {
                    if (this.status >= 200 && this.status < 400) {
                        jsonEstados = JSON.parse(this.response);
                        let selectEstados = document.querySelectorAll('.selectEstados');
                        let selectCidades = document.querySelectorAll('.selectCidades');
        
                        selectEstados.forEach((select, i) =>{
                            jsonEstados.estados.forEach(estado =>{
                                let option = document.createElement('option');
                                    option.value = estado.sigla;
                                    option.innerHTML = estado.nome;
                                select.appendChild(option);
                            });
                            select.addEventListener('change', event => {
                                let estadoAtual = event.target.value;
                                let cidades = jsonEstados.estados.filter(e => e.sigla == estadoAtual).shift().cidades;
                                selectCidades[i].innerHTML = '<option value="0">Cidade</option>';
                                cidades.forEach(cidade =>{
                                    let option = document.createElement('option');
                                        option.value = cidade;
                                        option.innerHTML = cidade;
                                    selectCidades[i].appendChild(option);
                                });
                            });
                        });
                    }
                };
                request.send();
        });
    }());
}