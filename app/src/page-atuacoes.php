<?php get_header(); 

/* Template Name: Atuações */

$tituloBox = get_field('titulo_fale_conosco', 'option');
$textoBox = get_field('texto_fale_conosco', 'option');

$pageID = $post->ID;


while ( have_posts() ) : the_post();   
    $title = get_the_title();
    
endwhile;       

?>	
    <main>
        <?php include 'modulos/header-page.php'; ?>
        <section class="container">
            <section class="content">
            <?php 
                    $argCategoria = array(
                        'post_type'  => 'page',
                        'posts_per_page' => -1,
                        'order' => 'ASC',
                        'orderby' => 'menu_order',
                        'post_parent' => $id
                    );
                    $categoria = new WP_Query( $argCategoria );
    
                    if( $categoria->have_posts() ): while( $categoria->have_posts() ) : $categoria->the_post();
    
                    $link = get_the_permalink();
                    $titulo = get_the_title();
                    $bgDesktop = get_field('header_desktop', $post->ID);
                    $bgMobile = get_field('header_mobile', $post->ID);
                
                    
                ?>
            <div class="card-atuacao">
                <div class="bg">
                    <img src="<?php echo $bgDesktop; ?>" class="hidden-xs" alt="<?php echo $title; ?>">
                    <img src="<?php echo $bgMobile; ?>" class="hidden-lg" alt="<?php echo $title; ?>">
                </div>
                <h2><?php echo $titulo; ?></h2>
                    <a href="<?php echo $link; ?>" class="btn greendark">Saiba mais</a>
            </div>
            <?php
                    endwhile; endif;
                    wp_reset_query();
                ?> 
            </section>
            <aside class="aside">
                <div class="box">
                    <h3 class="titulo titulo-box"><?php echo $tituloBox; ?></h3>
                    <p><?php echo $textoBox; ?></p>
                </div>
            </aside>
        </section>
    </main>
<?php get_footer(); ?> 
