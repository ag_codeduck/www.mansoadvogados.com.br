<?php
    $logoHeader = get_field('logo_header', 'option');

    $menuHeader = get_field('menu', 'option');

    $linkLogin = get_field('link_area_restrita', 'option');

    $redes = array(
        'facebook' => 'fa-facebook-f',
        'linkedin' => 'fa-linkedin',
        'instagram' => 'fa-instagram',
        'youtube' => 'fa-youtube',
        'flickr' => 'fa-flickr'
    );
?>

<header id="header">
        <input type="checkbox" id="burguer-menu" >
        <label for="burguer-menu">
            <span></span>
        </label>
        <a href="<?php echo $linkLogin; ?>" class="login" target="_blank"><i class="fas fa-user"></i></a>
        <div class="container">
            <div class="logo">
                <a href="<?php echo get_home_url(); ?>">
                    <img src="<?php echo $logoHeader; ?>">
                </a>
            </div>
            <nav>
                <div class="menu">
                    <?php foreach($menuHeader as $menu){
                        $texto = $menu['texto_menu'];
                        $linkAtual = $menu['link_menu'];
                    ?>
                    <a href="<?php echo $linkAtual; ?>">
                        <?php echo $texto; ?>
                    </a>
                    <?php } ?>
                </div>
            </nav>
        </div> 
</header>