<section class="breadcrumb hidden-xs">
    <div class="container">
        <ol class="breadcrumbs">
            <li><a href="<?php echo get_home_url(); ?>">Home</a></li>
            <?php if($tipo == "empresa"){ ?>
            <li><a href="<?php echo get_home_url(); ?>/representadas">Representadas</a></li>
            <li class="active"><?php the_title(); ?></li>
            <?php } else if($tipo == "categoria") {
                $linkParent = get_permalink($post->post_parent);    
                $tituloParent = get_the_title( $post->post_parent );    
                
            ?>
                <li><a href="<?php echo get_home_url(); ?>/representadas">Representadas</a></li>
                <li><a href="<?php echo $linkParent; ?>"><?php echo $tituloParent; ?></a></li>
                <li class="active"><?php the_title(); ?></li>
            <?php } else{?>
                <li class="active"><?php the_title(); ?></li>
            <?php } ?>
        </ol>
    </div>
</section>