<?php
    $tituloPadrao = get_field('titulo_padrao', 'option');
    $textoPadrao = get_field('texto_padrao', 'option');
    $emailPadrao = get_field('email_padrao', 'option');
    $telefonePadrao = get_field('telefone_padrao', 'option');
    $telefoneLink = preg_replace("/[^a-fA-F0-9]/", '', $telefonePadrao);

?>

<div class="contato">
    <div class="box sidebar contato">
        <h3 class="titulo sidebar"><?php echo $tituloPadrao; ?></h3>
        <p><?php echo $textoPadrao; ?></p>
        <span class="contatos"><i class="far fa-envelope mail"></i><a href="mailto:<?php echo strip_tags($emailPadrao); ?>"><?php echo $emailPadrao; ?></a></span>
        <br>
        <span class="contatos"><i class="fas fa-phone fone"></i><a href="tel:+55<?php echo $telefoneLink; ?>"><?php echo $telefonePadrao; ?></a></p></span>
    </div>
</div>