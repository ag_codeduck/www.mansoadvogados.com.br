<?php

if($tipo == "categoria"){
    $postID = wp_get_post_parent_id($id);
}else{
    $postID =  $post->ID;
}

    $logoEmpresa = get_field('logo_empresa', $postID);
    $paisEmpresa = get_field('pais_empresa', $postID);
    $textoAdicional = get_field('texto_empresa', $postID);
    $linkSite = get_field('site_empresa', $postID);
?>

<div class="detalhes">
    <img src="<?php echo $logoEmpresa; ?>" class="logo" alt="<?php echo $title; ?>">
    <p><?php echo $textoAdicional; ?></p>
    <p><strong>País:</strong> <?php echo $paisEmpresa; ?></p>
    <p><strong>Site: </strong>
    <?php if($linkSite){ ?>
            <a class="site" href="http://<?php echo $linkSite; ?>" target="_blank"><?php echo $linkSite; ?></a>
    <?php } ?>
    </p>
</div>