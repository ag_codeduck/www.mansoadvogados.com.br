<?php
    $tituloVegetalizacao = get_field('titulo_vegetalizacao', $post->ID);
    $textoVegetalizacao = get_field('texto_vegetalizacao', $post->ID);
    $linkVegetalizacao = get_field('link_vegetalizacao', $post->ID);
?>

<section class="vegetalizacao-home">
    <img data-svg="<?php echo get_template_directory_uri(); ?>/img/folha-bg.svg" class="svg"></a>
    <h2><?php echo $tituloVegetalizacao; ?></h2>
    <p><?php echo $textoVegetalizacao; ?></p>
    <a href="<?php echo $linkVegetalizacao; ?>" class="btn greendark">Saiba mais</a>
</section>