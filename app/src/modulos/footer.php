<?php

$textoFooter = get_field('texto_fale_conosco', 'option');

$redes = array(
    'facebook' => 'fa-facebook-f',
    'linkedin' => 'fa-linkedin',
    'instagram' => 'fa-instagram',
);

?>
<footer class="footer">
    <section class="infos">
        <?php echo $textoFooter; ?>
    </section>
    <section class="social-copy">
        <ul class="social-footer">
            <?php foreach($redes as $nome => $fonte){ ?>
                <?php $link = get_field('link_'.$nome , 'option'); ?>
                <?php if(!empty($link)){ ?>
                    <li>
                        <a href="<?php echo $link; ?>" target="_blank">
                            <i class="fab <?php echo $fonte; ?>"></i>
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
        <p>Todos os Direitos Reservados © 2009-<?php echo date("Y"); ?></p>
    </section>
</footer>