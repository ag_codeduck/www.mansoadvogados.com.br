<div class="produtos">
    <?php 
        $ecocert = get_field("logo_ecocert", "option");
        $rspo = get_field("logo_rspo", "option");
        $natrue = get_field("logo_natrue", "option");
        $produtos = get_field('produtos', $post->ID);

        foreach($produtos as $produto){
            $titulo = $produto['titulo_produto'];
            $inc = $produto['inci_produto'];
            $texto = $produto['texto_produto'];
            $selos = $produto['selos'];
            $imagem = $produto['imagem_produto'];
    ?>
            <div class="card-produto">
                <h2><?php echo $titulo; ?></h2>
                <p class="inc"><em><?php echo $inc; ?></em></p>
                <p><?php echo $texto; ?></p>
                <?php if($selos){ ?>
                    <div class="selos">
                        <?php for ($i=0; $i < count($selos) ; $i++) {
                            switch($selos[$i]){
                                case "ecocert":
                                echo '<img src="'.$ecocert.'">';
                                break;
                                case "rspo":
                                echo '<img src="'.$rspo.'">';
                                break;
                                case "natrue":
                                echo '<img src="'.$natrue.'">';
                                break;
                            }
                        }
                        ?>
                    </div>
                <?php } ?>
                <?php if($imagem){ ?>
                    <img src="<?php echo $imagem; ?>" class="destaque">
                <?php } ?>
            </div>
    <?php
        }
    ?>            
</div>
