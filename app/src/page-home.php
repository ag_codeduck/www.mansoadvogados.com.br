<?php get_header(); 

/* Template Name: Home */

$slider = get_field('slider', $post->ID);

$imagem_box_1 = get_field('imagem_box_1', $post->ID);
$imagem_box_2 = get_field('imagem_box_2', $post->ID);

$titulo_1_box1 = get_field('titulo_1_box1', $post->ID);
$texto_1_box_1 = get_field('texto_1_box_1', $post->ID);

$titulo_2_box1 = get_field('titulo_2_box1', $post->ID);
$texto_2_box_1 = get_field('texto_2_box_1', $post->ID);

$titulo_box2 = get_field('titulo_box2', $post->ID);
$texto_box_2 = get_field('texto_box_2', $post->ID);

$titulo_box3 = get_field('titulo_box3', $post->ID);
$texto_box_3 = get_field('texto_box_3', $post->ID);

?>	
    <main>
        <section class="content-home">
            <section  class="sliders">
                <section id="slideHome" class="slide-content"> 
                    <?php foreach ($slider as $slide) {
                        $imagem = $slide['imagem'];
                        $titulo = $slide['titulo'];
                        $chamada = $slide['chamada'];
                        $link = $slide['link'];
                    ?>
                    <div class="slide">
                        <div class="bg">
                            <img src="<?php echo $imagem;?>" alt="<?php echo $imagem;?>">
                        </div>
                        <div class="content-slide">
                            <h2><?php echo $titulo;?></h2>
                            <p><?php echo $chamada;?></p>
                            <a href="<?php echo $link;?>" class="btn">Saiba mais</a>
                        </div>
                    </div>
                    <?php } ?>
                </section>
                <section class="box3 hidden-xs">
                    <div class="content-box">
                        <h2 class="titulo titulo-home"><?php echo $titulo_box3; ?></h2>
                        <p><?php echo $texto_box_3; ?></p>
                    </div>
                </section>
            </section>
            <section class="boxes">
                <section class="box1">
                    <div class="content-box">
                        <h2 class="titulo titulo-home"><?php echo $titulo_1_box1; ?></h2>
                        <p><?php echo $texto_1_box_1; ?></p>
                        <h2 class="titulo titulo-home"><?php echo $titulo_2_box1; ?></h2>
                        <p><?php echo $texto_2_box_1; ?></p>
                    </div>
                    <div class="figure">
                        <img src="<?php echo $imagem_box_1; ?>" alt="<?php echo $titulo_1_box1; ?> | <?php echo $titulo_2_box1; ?>">
                    </div>
                </section>
                <section class="box2">
                    <div class="content-box">
                        <h2 class="titulo titulo-home"><?php echo $titulo_box2; ?></h2>
                        <p><?php echo $texto_box_2; ?></p>
                    </div>
                    <div class="figure">
                        <img src="<?php echo $imagem_box_2; ?>" alt="<?php echo $titulo_box2; ?>">
                    </div>
                </section>
                <section class="box3 hidden-lg">
                    <div class="content-box">
                        <h2 class="titulo titulo-home"><?php echo $titulo_box3; ?></h2>
                        <p><?php echo $texto_box_3; ?></p>
                    </div>
                </section>
                <section class="footer-home">
                    <?php include 'modulos/footer.php';?>
                </section>
            </section>
        </section>        
    </main>
<?php get_footer(); ?> 
